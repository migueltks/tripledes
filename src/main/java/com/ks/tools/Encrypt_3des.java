package com.ks.tools;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class Encrypt_3des
{
    Cipher cipher;

    private String keyHex;
    private String ecb_cbc_mode = "ECB";
    private String algo_mode = "TripleDES";
    private String cipherTransformation;


    public String encrypt(String message, String publicKey) throws Exception
    {
        this.keyHex = TripleDesPadding.tDesPadding(publicKey, algo_mode);
        SecretKey key = new SecretKeySpec(DatatypeConverter.parseHexBinary(keyHex), algo_mode);
        cipherTransformation = "DESede/ECB/NoPadding";
        cipher = Cipher.getInstance(cipherTransformation);

        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] result = cipher.doFinal(DatatypeConverter.parseHexBinary(message));
        return DatatypeConverter.printHexBinary(result);
    }
}
